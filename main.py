import flask
from flask import request, abort, jsonify
import random
import requests
from aseguradora import Aseguradora

class Main():

    def __init__(self):

        app = flask.Flask(__name__)
        app.config["DEBUG"] = True

        ws_config = {
            'aseguradora': 'http://127.0.0.1:5001',
            'oficina': 'http://127.0.0.1:5002',
            'subasta': 'http://127.0.0.1:5003',
            'jwt': 'http://127.0.0.1:5004',
        }
        aseguradora = Aseguradora(ws_config)

        app.run(port=6000)